# Pour visualiser ces fichiers sans rien installer

Aller sur https://nbviewer.jupyter.org/ puis copier l'adresse du fichier notebook `.ipynb` *suivie de* `?inline=false` (ou copier l'adresse du
lien depuis le bouton «télécharger» sur la page du fichier notebook).

# Pour une utilisation interactive 
## En ligne, sans installation ni téléchargement ni `git pull`
(Grace à binder.org merci ! Mais du coup, il faut patienter au démarrage...)

C'est [par là](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fjmg74%2Fmy_mess/master?filepath=Jupyter_Notebooks)

## Sur sa machine, avec Jupyter (télécharger les notebooks)
Voir https://jupyter.readthedocs.io/en/latest/ et en particulier https://jupyter-notebook.readthedocs.io/en/latest/

Le plus pratique est d'installer https://www.anaconda.com/distribution/ pour disposer directement de tous les outils