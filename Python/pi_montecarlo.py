from math import *
from random import *
import matplotlib.pyplot as plt

def distance(x1, y1, x2, y2):
    return sqrt((x2-x1)**2+(y2-y1)**2)

def distance_pt(A, B):
    return distance(A[0], A[1], B[0], B[1])


def pi_MonteCarlo(n):
    on_disk = 0
    for i in range(n):
        x,y = 2*random()-1, 2*random()-1
        if distance(0,0, x,y) <= 1:
            on_disk += 1
            plt.plot(x,y, 'g.')
        else:
            plt.plot(x,y, 'r.')
    plt.grid()
    plt.show()
    return 4*on_disk/n

if __name__ == "__main__":
   
    N = 5000
    print(pi_MonteCarlo(5000))
