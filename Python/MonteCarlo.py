from math import sqrt, pi
from random import random
import matplotlib.pyplot as plt

def distance(x1, y1, x2, y2):
    """
    Return distance between (x1,y1) and (x2,y2), in orthonormal 2D landmark
    """
    return sqrt((x2-x1)**2 + (y2-y1)**2)

def pi_montecarlo(n, graphdisplay=False):
    """
    Return approximative value of Pi number, with n iterations of Monte Carlo method
     
    """
    on_disk = 0
    for i in range(n): 
        x,y = 2*random()-1, 2*random()-1
        if distance(0,0, x,y) <= 1:
            on_disk += 1
            if graphdisplay:
                # Green point
                plt.plot(x,y, 'g.')
        elif graphdisplay:
            # Red point
            plt.plot(x,y, 'r.')
    if graphdisplay:
        plt.grid()
        plt.show()
    return 4*on_disk/n

if __name__ == "__main__":
    
    N = 4000
    GRAPH = False
    
    if GRAPH:
        print("Be patient, could be long...")
        print("Then close graphic window if needed to get your pi ;-)")
    pi_mc = pi_montecarlo(N, GRAPH)
    print("π ≈ {:5f} ({:4f}% error)".format(pi_mc, abs(100 * (pi_mc-pi) / pi)))
