i = 42

def f():
    i = 0
    def h(delta):
        nonlocal i
        i += delta
        return i
    return h

g = f()
gg = f()

print("scope de g : i += 1 => i = ", g(1))
print("scope de g : i += 5 => i = ", g(5))
print("scope de g : i += -1 => i = ", g(-1))
print("scope de gg : i += 1 => i = ", gg(1))
print("scope de gg : i += 3 => i = ", gg(3))

print("i global = ",i)
