"""func_test() is provided, to automate a function test bench

It uses 3 special Unicode characters so feel free to replace them by
'|--->', 'ok' and 'no', or even to delete them, in case of print issues
(see #### lines).
"""

def single_test(func, *args, printing=True):
    """Tests a function for a unique example. Returns boolean result.

    Usage: single_test(my_func, arg1, arg2, ..., argn, output)
    Prints explanation if and only if printing==True.

    Main goal is not a direct use, hbut mostly as part of func_test()
    """
    if printing:
        # Arguments
        for i in range(len(args[:-1])):
            print(args[i], end="")
            # Not the last argument ?
            if i < len(args[:-1]) - 1 :
                print(", ", end="")
        # Result
        print("\t↦ ", func(args[:-1]), #### \u21A6 \u2714 \u2718 ####
              "✔" if func(args[:-1]) == args[-1] \
              else " (should be "+str(args[-1])+") ✘") 
        
    return True if func(args[:-1]) == args[-1] else False


def func_test(func, *args, printing=True):
    """Tests a function with given inputs/output. Returns a boolean.

    *args represents either one batch, either a sequence of
          inputs/output batches.
    Computings are printed if and only if printing==True.
    
    Usages :
    . Unique test
             func_test(func, arg1, ..., argn, output)
             func_test(func, arg1, ..., argn, output, printing=False)
    . Or sequence of tests
             func_test(func,
                       (arg1, ..., argn, output),
                       (arg_1, ..., arg_n, output_), ...,
                       printing=False)
             func_test(func,
                       [arg1, ..., argn, output],
                       [arg_1, ..., arg_n, output_])
    """
    # Unique test, args doesn't contain iterables yet ?
    try:
        for e in args[0]: break
    except TypeError:
        # Convert to tuple
        args = (args,)

    # Tests loop
    if printing:
        print("Testing ", func.__name__, "():", sep="")
    ans = True
    for arg in args:
        if not single_test(func, *arg, printing=printing):
            # return False   here => not all tests shown
            ans = False
    return ans
