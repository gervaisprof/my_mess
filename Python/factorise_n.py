#from numba import jit

#@jit
def diviseur_suivant(d): # À optimiser ?
    return 3 if d == 2 else d+2

#@jit
def factorisation(n):
    q = n
    div = 2
    mult_div = 0
    
    # n = ...
    txt = "{:d} = ".format(n)

    while q > 1:
        if q % div == 0:
            q = q // div
            if mult_div == 0:
                mult_div = 1
                # ajout du diviseur
                txt += "{:d}".format(div)
            else:
                mult_div += 1
        else:
            if mult_div > 0:
                if mult_div > 1:
                    # Ajout de l'exposant / multiplicité
                    txt += "^{}".format(mult_div)
                if q > 1:
                    # Pas fini : passage au diviseur suivant
                    txt += " × "
            mult_div = 0
            # Externalisé pour pouvoir optimiser
            #div = 3 if div == 2 else div + 2
            div = diviseur_suivant(div)
    return txt


if __name__ == "__main__":
    for i in range(2411500, 2411509):
        print(factorisation(i))
    print("\n", factorisation(2969694), sep="")


        
    
